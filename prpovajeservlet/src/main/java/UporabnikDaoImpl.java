import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UporabnikDaoImpl implements BaseDao {
    private static UporabnikDaoImpl ourInstance = new UporabnikDaoImpl();

    public static UporabnikDaoImpl getInstance() {
        return ourInstance;
    }

    private UporabnikDaoImpl() {
    }
    @Override
    public Connection getConnection() throws NamingException, SQLException {
        InitialContext initCtx = new InitialContext();
        DataSource ds = (DataSource) initCtx.lookup("jdbc/SimpleJdbcDS");
        return ds.getConnection();
    }

    @Override
    public Uporabniki vrni(int id) {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {

            if (con == null) {
                con = getConnection();
            }

            String sql = "SELECT * FROM uporabnik WHERE id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return getUporabnikFromRS(rs);
            } else {
                //log.info("Uporabnik ne obstaja");
            }

        } catch (SQLException e) {
//            log.severe(e.toString());
        } catch (NamingException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
//                    log.severe(e.toString());
                }
            }
        }
        return null;
    }

    @Override
    public void vstavi(Uporabniki ent) {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {

            if (con == null) {
                con = getConnection();
            }

            String sql = "INSERT INTO uporabniki (ime, priimek, uporabniskoIme, email) VALUES (?,?,?,?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, ent.getIme());
            ps.setString(2, ent.getPriimek());
            ps.setString(3, ent.getUporabniskoIme());
            ps.setString(4, ent.getEmail());
            ResultSet rs = ps.executeQuery();


        } catch (SQLException e) {
            //log.severe(e.toString());
        } catch (NamingException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
//                    log(e.toString());
                }
            }
        }
    }

    @Override
    public void odstrani(int id) {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {

            if (con == null) {
                con = getConnection();
            }

            String sql = "DELETE FROM uporabniki WHERE id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();


        } catch (SQLException e) {
            //log.severe(e.toString());
        } catch (NamingException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
//                    log(e.toString());
                }
            }
        }
    }

    @Override
    public void posodobi(Entiteta ent) {

    }

    @Override
    public List<Uporabniki> vrniVse() {
        PreparedStatement ps = null;
        List<Uporabniki> tmp = new ArrayList<>();
        Connection con = null;
        try {
            con = getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {

            if (con == null) {
                con = getConnection();
            }

            String sql = "SELECT * FROM uporabniki";
            ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();


            while (rs.next()) {
                tmp.add(getUporabnikFromRS(rs));
            }

        } catch (SQLException e) {
            //log.severe(e.toString());
        } catch (NamingException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
//                    log(e.toString());
                }
            }
        }
        return tmp;
    }

    private Uporabniki getUporabnikFromRS(ResultSet rs) throws SQLException {

        String ime = rs.getString("ime");
        String priimek = rs.getString("priimek");
        String uporabniskoIme = rs.getString("uporabniskoime");
        String email = rs.getString("email");
        return new Uporabniki(ime, priimek, uporabniskoIme, email);

    }
}
