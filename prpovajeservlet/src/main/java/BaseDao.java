import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface BaseDao {
    Connection getConnection() throws NamingException, SQLException;

    Entiteta vrni(int id);

    void vstavi(Uporabniki ent);

    void odstrani(int id);

    void posodobi(Entiteta ent);

    List<Uporabniki> vrniVse();
}
