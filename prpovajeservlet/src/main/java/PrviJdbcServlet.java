import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

@WebServlet("/servlet")
public class PrviJdbcServlet extends HttpServlet {
    private String message;

    public void init() throws ServletException {
        // Do required initialization
        message = "Hello World";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Set response content type
        response.setContentType("text/html");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println("<h1>" + message + "</h1>");

       // UporabnikDaoImpl.getInstance().vstavi(new Uporabniki("tilen", "tilen", "ti", "t@si" ));
        //UporabnikDaoImpl.getInstance().odstrani(5);


        List<Uporabniki> uporabnik = UporabnikDaoImpl.getInstance().vrniVse();
        if (uporabnik == null || uporabnik.isEmpty())
            response.getWriter().println("No uporabnik found.");
        else {
            for (Uporabniki uporabniki : uporabnik) {
                response.getWriter().println(uporabniki.toString());
                response.getWriter().println();
                log(uporabniki.toString());
            }
        }

       // response.getWriter().println( UporabnikDaoImpl.getInstance().vrni(1).toString());
    }
}
