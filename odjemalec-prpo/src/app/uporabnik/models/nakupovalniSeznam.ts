import { Artikel } from "./artikel";

export class NakupovalniSeznam {
    id: number;
    naziv: string;
    opis: string;
    status: string;
    artikelList: Array<Artikel>;
}
