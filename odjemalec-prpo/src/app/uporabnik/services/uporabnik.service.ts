import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import { Observable } from 'rxjs';

import { catchError } from 'rxjs/operators';
import { NakupovalniSeznam } from '../models/nakupovalniSeznam';

@Injectable()
export class UporabnikService {

    private headers = new HttpHeaders({'Content-Type': 'application/json'});
    private url = 'http://localhost:8080/v1/nakupovalniSeznam';

    constructor(private http: HttpClient) {
    }

    getUporabniki(): Observable<NakupovalniSeznam[]> {
        return this.http.get<NakupovalniSeznam[]>(this.url)
                        .pipe(catchError(this.handleError));
    }

    getUporabnik(id: number): Observable<NakupovalniSeznam> {
        const url = `${this.url}/${id}`;
        return this.http.get<NakupovalniSeznam>(url)
                        .pipe(catchError(this.handleError));
    }

    delete(id: number): Observable<null> {
        const url = `${this.url}/${id}`;
        return this.http.delete<null>(url, {headers: this.headers})
                        .pipe(catchError(this.handleError));
    }

    create(uporabnik: NakupovalniSeznam): Observable<NakupovalniSeznam> {
        return this.http.post<NakupovalniSeznam>(this.url, JSON.stringify(uporabnik), {headers: this.headers})
                        .pipe(catchError(this.handleError));
    }

    private handleError(error: any): Promise<any> {
        console.error('Prišlo je do napake', error);
        return Promise.reject(error.message || error);
    }
}

