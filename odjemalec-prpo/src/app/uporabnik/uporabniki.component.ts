import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {UporabnikService} from './services/uporabnik.service';
import { NakupovalniSeznam } from './models/nakupovalniSeznam';

@Component({
    moduleId: module.id,
    selector: 'vsi-uporabniki',
    templateUrl: 'uporabniki.component.html'
})
export class UporabnikiComponent implements OnInit {
    uporabniki: NakupovalniSeznam[];
    uporabnik: NakupovalniSeznam;

    constructor(private uporabnikService: UporabnikService,
                private router: Router) {
    }

    ngOnInit(): void {
        this.getUporabniki();
    }

    getUporabniki(): void {
        this.uporabnikService
            .getUporabniki()
            .subscribe(uporabniki => this.uporabniki = uporabniki);
    }

    naPodrobnosti(uporabnik: NakupovalniSeznam): void {
        this.uporabnik = uporabnik;
        this.router.navigate(['/nakupovalniSeznam', this.uporabnik.id]);
    }

    delete(uporabnik: NakupovalniSeznam): void {
        this.uporabnikService
            .delete(uporabnik.id)
            .subscribe(uporabnikId => this.uporabniki = this.uporabniki.filter(u => u.id !== uporabnikId));
            this.router.navigate(['/nakupovalniSeznam']);
        }

    dodajUporabnika(): void {
        this.router.navigate(['/dodajuporabnika']);
    }
    refresh(): void {
        this.router.navigate(['/nakupovalniSeznam']);
    }

}
