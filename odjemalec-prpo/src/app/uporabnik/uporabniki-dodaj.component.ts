import {Component} from '@angular/core';
import {Router} from '@angular/router';

import {UporabnikService} from './services/uporabnik.service';
import { NakupovalniSeznam } from './models/nakupovalniSeznam';

@Component({
    moduleId: module.id,
    selector: 'dodaj-uporabniki',
    templateUrl: 'uporabniki-dodaj.component.html'
})
export class UporabnikiDodajComponent {
    uporabnik: NakupovalniSeznam = new NakupovalniSeznam;

    constructor(private uporabnikService: UporabnikService,
                private router: Router) {
    }

    submitForm(): void {
        this.uporabnikService.create(this.uporabnik)
        .subscribe(() => this.router.navigate(['/nakupovalniSeznam']));
    }

    nazaj(): void {
        this.router.navigate(['/nakupovalniSeznam']);
    }

}
