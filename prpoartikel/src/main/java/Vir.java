import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/artikel")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class Vir {

//    @Inject
//    private Zrno zrno;



    @GET
    public Response vrniArtikel(){
        List<Artikel> listArtikel = new ArrayList<Artikel>();
        listArtikel.add(new Artikel(0,"Kruh","Bel kruh", "", "",1));
        listArtikel.add(new Artikel(1,"Čaj","šipkov", "","",1));
        listArtikel.add(new Artikel(2,"Mleko","Aplsko", "","",1));
        listArtikel.add(new Artikel(3,"Maslo","Lecclerc", "","",2));
        listArtikel.add(new Artikel(4,"Kruh","Črn kruh", "","",1));

        return Response.status(Response.Status.OK).entity(listArtikel).build();
    }
    @GET
    @Path("/{id}")
    public Response vrniArtikel(@PathParam("id") Integer id){
        Artikel artikel = new Artikel();
        List<Artikel> listArtikel = new ArrayList<Artikel>();
        listArtikel.add(new Artikel(0,"Kruh","Bel kruh", "","",1));
        listArtikel.add(new Artikel(1,"Čaj","šipkov", "","",1));
        listArtikel.add(new Artikel(2,"Mleko","Aplsko", "","",1));
        listArtikel.add(new Artikel(3,"Maslo","Lecclerc", "","",2));
        listArtikel.add(new Artikel(4,"Kruh","Črn kruh", "","",1));

        for (Artikel a: listArtikel) {
            if(a.getId() == id){
                artikel = a;
            }
        }

        return Response.status(Response.Status.OK).entity(artikel).build();
    }
}
