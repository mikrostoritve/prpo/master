
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class Zrno {

    List<Artikel> listArtikel;
    @PostConstruct
    public void start() {
        listArtikel = new ArrayList<Artikel>();
        listArtikel.add(new Artikel(0,"Kruh","Bel kruh", "","", 1));
        listArtikel.add(new Artikel(1,"Čaj","šipkov", "","",1 ));
        listArtikel.add(new Artikel(2,"Mleko","Aplsko", "","",1));
        listArtikel.add(new Artikel(3,"Maslo","Lecclerc", "","",2));
        listArtikel.add(new Artikel(4,"Kruh","Črn kruh", "","",1));
        listArtikel.add(new Artikel(5,"Kruh","Črn kruh", "","",1));
    }


    public List<Artikel> getListArtikel(){
        return listArtikel;
    }
    public Artikel getArtikel(Integer id){
        for (Artikel a: listArtikel) {
            if(a.getId() == id){
                return a;
            }
        }
        return null;
    }

}
