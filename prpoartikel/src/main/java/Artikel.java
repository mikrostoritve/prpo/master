public class Artikel {
    private Integer id;

    private String naziv;

    private String opis;

    private String status;

    private String cena;

    private Integer NakuSez;

    public String getCena() {
        return cena;
    }

    public void setCena(String cena) {
        this.cena = cena;
    }

    public Integer getNakuSez() {
        return NakuSez;
    }

    public void setNakuSez(Integer nakuSez) {
        NakuSez = nakuSez;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Artikel{" +
                "id=" + id +
                ", naziv='" + naziv + '\'' +
                ", opis='" + opis + '\'' +
                ", status='" + status + '\'' +
                ", cena='" + cena + '\'' +
                ", NakuSez=" + NakuSez +
                '}';
    }

    public Artikel() {
    }

    public Artikel(Integer id, String naziv, String opis, String status, String cena, Integer nakuSez) {
        this.id = id;
        this.naziv = naziv;
        this.opis = opis;
        this.status = status;
        this.cena = cena;
        NakuSez = nakuSez;
    }
}
